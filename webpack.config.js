const path = require('path');
const webpack = require('webpack'); // to access built-in plugins

// @note: hot reload code taken from : https://webpack.js.org/guides/hmr-react/

const config = {

  entry: [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/only-dev-server',
    './App.js'
  ],

  output: {
    path: path.resolve(__dirname, 'dist_dev/0.0.2/js/'),
    filename: 'bundle.js',
    publicPath: 'http://localhost:8080/'
  },

  context: path.resolve(__dirname, 'assets/js/'),

  devtool: 'inline-source-map',

  devServer: {
    hot: true,
    // enable HMR on the server

    contentBase: path.resolve(__dirname, 'dist_dev/0.0.2/js/'),
    // match the output path

    publicPath: 'http://localhost:8080/'
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader'
      },
      {
        test: /\.scss$/,
        use: [{
            loader: "style-loader" // creates style nodes from JS strings
        }, {
            loader: "css-loader" // translates CSS into CommonJS
        }, {
            loader: "sass-loader" // compiles Sass to CSS
        }]
      }
    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
    // new webpack.optimize.UglifyJsPlugin()
  ]

};

module.exports = config;
