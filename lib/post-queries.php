<?php

/**
 * Get recent projects
 */

function smash_get_portraits($post_per_page = -1) {

	// get posts
    $posts = new WP_Query(array(
        'post_type'      => 'post',
        'posts_per_page' => $post_per_page,
		'orderby'        => 'menu_order'
    ));

	return $posts;
}


?>
