<?php

function yoast_change_title( $title ) {
    $page_content_labels = Array(
        "interview" => "Entrevue",
        "gallery" => "Galerie",
        "video" => "Vidéo"
    );

    // get page content param
    $page_content = $_GET['page_content'];

    // if page content param is set, set before title with SEO sep
    if ($page_content_labels[$page_content] && function_exists(wpseo_replace_vars)) {
        $title = $page_content_labels[$page_content] . ' ' . wpseo_replace_vars("%%sep%%", false) . ' ' . $title;
    }
    // } elseif (function_exists(wpseo_replace_vars))  {
    //     // $title = $title . ' ' . wpseo_replace_vars("%%sep%% %%sitename%%", false);
    // }

    // return formated title
    return $title;
}

add_filter( 'wpseo_title', 'yoast_change_title', 10, 1 );

function yoast_change_opengraph_image($image) {
    global $post;

    // get page content param
    $page_content = $_GET['page_content'];

    // get video preview image
    $video_preview_image = get_field('driver_video_preview_image', $post);

    // check if video and has video preview image, or return original og:image
    if ($page_content == 'video' && $video_preview_image) {

        $image = $video_preview_image['sizes']['large'];
        return $image;
    } else {
        // return original og:image
        return $image;
    }


}

add_filter( 'wpseo_opengraph_image', 'yoast_change_opengraph_image', 10, 1 );

// disable canonical
function yoast_disable_canonical( $string ) {
    return isset($_GET['page_content']) ? false : $string;
}

add_filter( 'wpseo_canonical', 'yoast_disable_canonical', 10, 1 );


// add_filter('query_vars', 'add_state_var', 0, 1);
// function add_state_var($vars){
//     $vars[] = 'video';
//     return $vars;
// }
// add_rewrite_rule('^portraits/([^/]*)/([^/]*)/?','index.php?post_type=post&name=$matches[1]&state=$matches[2]','top');


?>
