<?php

namespace Roots\Sage\Assets;

/**
* Return path for assets based on theme version
*/
function get_theme_version_assets_path() {
    $theme = wp_get_theme();
    $version = $theme->get("Version");
    return $version;
}

function get_asset_root($echo = true) {
    $theme = wp_get_theme();
    $version = $theme->get("Version");
    if ($echo) {
        echo asset_path();
    } else {
        return asset_path();
    }
}

/**
* Return asset path with current theme version
*/
function asset_path($filename = null) {
    $dist_name = THEME_DEV === true ? '/dist_dev/' : '/dist/';
    $version   = get_theme_version_assets_path();
    $dist_path = get_template_directory_uri() . $dist_name . $version . '/';

    if ($filename) {
        $directory = dirname($filename) . '/';
        $file      = basename($filename);

        return $dist_path . $directory . $file;
    } else {
        return $dist_path;
    }
}
