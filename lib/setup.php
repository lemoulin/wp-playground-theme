<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

/**
* Theme setup
*/
function setup() {
    // Enable features from Soil when plugin is activated
    // https://roots.io/plugins/soil/
    add_theme_support('soil-clean-up');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-relative-urls');

    // Make theme available for translation
    // Community translations can be found at https://github.com/roots/sage-translations
    load_theme_textdomain('sage', get_template_directory() . '/lang');

    // Enable plugins to manage the document title
    // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
    add_theme_support('title-tag');

    // Register wp_nav_menu() menus
    // http://codex.wordpress.org/Function_Reference/register_nav_menus
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'sage')
    ]);

    // Enable post thumbnails
    // http://codex.wordpress.org/Post_Thumbnails
    // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
    // http://codex.wordpress.org/Function_Reference/add_image_size
    add_theme_support('post-thumbnails');
    add_image_size( 'video', 704, 480, true );
    add_image_size( 'pixel', 100 );
    add_image_size( 'largest', 1800 );
    add_image_size( 'largest-2x', 2400 );

    // Enable post formats
    // http://codex.wordpress.org/Post_Formats
    //add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

    // Enable HTML5 markup support
    // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    // Use main stylesheet for visual editor
    // To add custom styles edit /assets/styles/layouts/_tinymce.scss
    // add_editor_style(Assets\asset_path('styles/main.css'));

    // add ACF Option page
    if( function_exists('acf_add_options_page') ) {
        acf_add_options_page(array(
            'page_title' 	=> 'Contenu & Options du thème',
            'menu_title'	=> 'Options - Smashs',
            'menu_slug' 	=> 'theme-general-settings'
        ));
    }

}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
* Register sidebars
*/
function widgets_init() {
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ]);

    register_sidebar([
        'name'          => __('Footer', 'sage'),
        'id'            => 'sidebar-footer',
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
* Determine which pages should NOT display the sidebar
*/
function display_sidebar() {
    static $display;

    return apply_filters('sage/display_sidebar', $display);
}

/**
* Theme assets
*/
function assets() {
    // wp_enqueue_style('sage/css', Assets\asset_path('css/main.css'), false, null);

    wp_enqueue_script('sage/jquery', Assets\asset_path('js/vendors/jquery/jquery.min.js'), null, true);
    // wp_enqueue_script('sage/js', Assets\asset_path('js/bundle.js'), null, true);
    wp_enqueue_script('sage/js', "http://localhost:8080/bundle.js", null, true);

    wp_deregister_script('jquery');
    wp_deregister_script('jquery-migrate');
}
add_action( 'get_footer', __NAMESPACE__ . '\\assets', 1 );
// add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);


/**
* 'nough said..
*/
function disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}

add_filter( 'emoji_svg_url', '__return_false' );
add_action( 'init', __NAMESPACE__ . '\\disable_wp_emojicons' );
