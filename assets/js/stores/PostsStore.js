import Reflux from 'reflux';
import PostsActions from '../actions/PostsActions.js';
import got from 'got';

let PostsStore = Reflux.createStore({
  listenables: [PostsActions],
  data: [],
  onLoadPosts: function() {
    got("/wp-json/wp/v2/posts",
      {
        json: true,
        query: {
          per_page: 100
        }
      })
      .then(response => {
        PostsActions.loadPosts.completed(response.body)
      })
      .catch(error => {
        PostsActions.loadPosts.failed(error);
      });
  },
  onLoadPostsCompleted: function(data) {
    this.trigger(this.data = data);
  },
  onLoadPostsFailed: function(err) {
    this.trigger(this.data = []);
  },
  getInitialState: function() {
    return this.data;
  }
});

export default PostsStore;
