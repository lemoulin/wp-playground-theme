import Reflux from 'reflux';
import PostActions from '../actions/PostActions.js';
import got from 'got';

let PostStore = Reflux.createStore({
  listenables: [PostActions],
  data: [],
  onLoadPost: function(slug) {
    got("/wp-json/wp/v2/posts", {
        json: true,
        query: {
          slug: slug,
          status: "publish"
        }
      })
      .then(response => {
        PostActions.loadPost.completed(response.body[0])
      })
      .catch(error => {
        PostActions.loadPost.failed(error);
      });
  },
  onLoadPostCompleted: function(data) {
    console.log(data);
    this.trigger(this.data = data);
  },
  onLoadPostFailed: function(err) {
    this.trigger(this.data = []);
  },
  getInitialState: function() {
    return this.data;
  }
});

export default PostStore;
