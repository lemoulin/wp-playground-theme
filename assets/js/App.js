import '../sass/main.scss';

// Node modules
// ********************
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, Link, browserHistory } from 'react-router'
import { AppContainer } from 'react-hot-loader';

// components for routes
import Main from "./components/Main";
import About from "./components/About";
import Post from "./components/Post"

const render = (Component) => {
  ReactDOM.render(
    <AppContainer>
      <Router history={browserHistory}>
        <Route path="/" component={Main} />
        <Route path="/about/" component={About}/>
        <Route path="/posts/:slug" component={Post}/>
        <Route path="*" component={Main}/>
      </Router>
    </AppContainer>,
    document.getElementById('root')
  );
};

// call render method
render(Main);


// Hot Module Replacement API
if (module.hot) {
  // will accept all components
  module.hot.accept();
}
