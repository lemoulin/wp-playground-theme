import Reflux from 'reflux';

var PostsActions = Reflux.createActions({
  "loadPosts": {asyncResult: true}
});

export default PostsActions;
