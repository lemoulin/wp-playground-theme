import Reflux from 'reflux';

var PostActions = Reflux.createActions({
  "loadPost": {asyncResult: true}
});

export default PostActions;
