import React from 'react';

import Nav from "./Nav";

class Base extends React.Component {
  render() {
    return (
      <main id="main">
        <Nav/>
        {this.props.children}
      </main>
    );
  }
}

export default Base;
