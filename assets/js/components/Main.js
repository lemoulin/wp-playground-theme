// Node modules
// ********************
import React from 'react';

import Base from "./Base";
import RecentPosts from "./RecentPosts";

const Main = () => (
  <Base>
    <RecentPosts/>
  </Base>
);

export default Main;
