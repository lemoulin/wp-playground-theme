import React from 'react';
import { Link } from 'react-router'

import PostMedia from './PostMedia';

class PostItem extends React.Component {
  render() {
    return (
      <div className="column is-half">
        <article className="box">
          <div className="media">
            <div className="media-left">
            { this.props.post.better_featured_image ?
              <PostMedia src={this.props.post.better_featured_image.media_details.sizes.thumbnail.source_url} alt={this.props.post.better_featured_image.alt_text} sizes="image is-120x120" />
            :
              <PostMedia src="https://placeholdit.imgix.net/~text?txtsize=33&txt=no-image&w=150&h=150" alt={this.props.post.title.rendered} sizes="image is-120x120" />
            }
            </div>
            <div className="media-content">
              <h4 key={this.props.post.id}><Link to={`/posts/${this.props.post.slug}`}>{this.props.post.title.rendered}</Link></h4>
              <p>
                  <Link to={`/posts/${this.props.post.slug}`} className="button is-primary is-outlined">Read more</Link>
              </p>
            </div>
          </div>
        </article>
      </div>
    );
  }
}

export default PostItem;
