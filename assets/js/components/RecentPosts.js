import React from 'react';
import ReactDOM from 'react-dom';
import Reflux from 'reflux';
import { Link } from 'react-router'
import got from 'got';

import PostsStore from '../stores/PostsStore.js';
import PostsActions from '../actions/PostsActions.js';
import PostItem from './PostItem';

// object for posts
// let postsData = null;

// Projects container
const RecentPosts = React.createClass({

  mixins: [Reflux.connect(PostsStore,"posts")],

  componentWillMount() {
    PostsActions.loadPosts();
  },

  componentDidMount() {
    // console.warn("mounting...", this.state);
  },

  postEntries() {
    return (
      this.state.posts.map((post, index) =>
        <PostItem post={post} key={index} />
      )
    );
  },

  render() {
      return (
        <div>
          <section className="hero is-primary">
            <div className="hero-body">
              <div className="container">
                <h1 className="title">
                    Recent posts
                </h1>
              </div>
            </div>
          </section>
          <div className="columns is-multiline post-content">
              { this.postEntries() }
          </div>
        </div>
      );
  }
});

export default RecentPosts;
