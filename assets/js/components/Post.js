import React from 'react';
import ReactDOM from 'react-dom';
import Reflux from 'reflux';
// import got from 'got';

import Base from "./Base";
import PostStore from '../stores/PostStore.js';
import PostActions from '../actions/PostActions.js';

const Post = React.createClass({

  mixins: [Reflux.connect(PostStore,"post")],

  componentDidMount() {
    PostActions.loadPost(this.props.params.slug);
  },

  componentWillMount() {
    // always empty post when mounting..
    this.setState({
      "post": {}
    })
  },

  postTitle() {
    return this.state.post.title ? this.state.post.title.rendered : ""
  },

  postContent() {
    return {__html : this.state.post.content ? this.state.post.content.rendered : "" }
  },

  render() {
    return (
      <Base>
        <section className="hero is-primary">
          <div className="hero-body">
            <div className="container">
              <h1 className="title">{this.postTitle()}</h1>
            </div>
          </div>
        </section>
        <div dangerouslySetInnerHTML={this.postContent()} className="post-content" />
      </Base>
    )
  }

});

export default Post;
