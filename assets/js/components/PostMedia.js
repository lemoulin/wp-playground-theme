import React from 'react';

class PostMedia extends React.Component {
  render() {
    return (
      <figure className={this.props.sizes}>
        <img src={this.props.src} alt={this.props.alt} />
      </figure>
    );
  }
}

export default PostMedia;
