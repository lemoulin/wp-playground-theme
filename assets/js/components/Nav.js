import React from 'react';
import { Link } from 'react-router'

class Nav extends React.Component {
    render() {
        return (
            <nav id="main-nav" className="nav has-shadow">
                <div className="container">
                    <div className="nav-left">
                        <Link to={`/`} className="nav-item is-tab" activeClassName="is-active">Home</Link>
                        <Link to={`/about/`} className="nav-item is-tab" activeClassName="is-active">About</Link>
                        <Link to={`/posts/soluta-saepe-eligendi-est-eligendi-mollitia-aut/`} className="nav-item is-tab" activeClassName="is-active">One Post</Link>
                        <Link to={`/posts/*`} className="nav-item is-tab" activeClassName="is-active">All Posts</Link>
                    </div>
                </div>
            </nav>
        );
    }
}

export default Nav;
