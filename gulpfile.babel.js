/*
*  get package version
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
const npm_package = process.env;
const version     = npm_package.npm_package_version;

/*
*  import utils
* ~~~~~~~~~~~~~
*/
import notifier from 'node-notifier';
import gulp from 'gulp';
import gulpif from 'gulp-if';
import header from 'gulp-header';
import copy from 'gulp-copy';
import hash from 'gulp-hash';
import sassGlob from 'gulp-sass-glob';
import cssimport from 'gulp-cssimport';
import del from 'del';
import plumber from 'gulp-plumber';
import babelify from 'babelify';
import sass from 'gulp-sass';
import imagemin from 'gulp-imagemin';
import source from 'vinyl-source-stream';
import concat from 'gulp-concat';
import consolidate from 'gulp-consolidate';
import minifyCSS from 'gulp-clean-css';
import rename from 'gulp-rename';
import uglify from 'gulp-uglify';
import gutil from 'gulp-util';
import path from 'path';
import livereload from 'gulp-livereload';
import iconfont from 'gulp-iconfont';
import sort from 'gulp-sort';

/*
*  webpack related
* ~~~~~~~~~~~~~~~~
*/
import named from 'vinyl-named';
import webpack from 'webpack';
import webpackStream from 'webpack-stream';

/*
* Global variables
* ~~~~~~~~~~~~~~~~
*/
const project_name = 'moulin-wp-playground';
const root_dir = __dirname;
const assets_dir = path.join(root_dir, 'assets');
const PROD_ENV = gutil.env.production;

/*
* DIRS
* ~~~~
*/
const build_dir    = PROD_ENV ? path.join('dist', version) : path.join('dist_dev', version);
const bower_dir    = path.join(root_dir, 'bower_components');
const npm_dir      = path.join(root_dir, 'node_modules');
const css_dir      = path.join(assets_dir, 'css');
const sass_dir     = path.join(assets_dir, 'sass');
const js_dir       = path.join(assets_dir, 'js');
const img_dir      = path.join(assets_dir, 'img');
const svg_dir      = path.join(assets_dir, 'svg');
const font_dir     = path.join(assets_dir, 'fonts');
const iconfont_dir = path.join(assets_dir, 'iconfont');
const main_css     = path.join(css_dir, 'main.css');
const main_sass    = path.join(sass_dir, 'main.scss');
const editor_sass  = path.join(sass_dir, 'editor.scss');
const theme_dir    = '/wp-content/themes/smash/';

/*
* Templates for gulp-header
* ~~~~~~~~~~~~~~~~~~~~~~~~~
*/
var banner = {
    theme :
    `/**
    * Theme Name: ${npm_package.npm_package_name}
    * Theme URI: ${npm_package.npm_package_homepage}
    * GitHub Theme URI: __
    * Description: ${npm_package.npm_package_description}
    * Version: ${npm_package.npm_package_version}
    * Author: ${npm_package.npm_package_author_name}
    * Author URI: ${npm_package.npm_package_homepage}
    * License: Commercial
    * Open Source Credits: None
    */`
};

/*
* Webpack config
* ~~~~~~~~~~~~~~
*/
var webpackConfig = {
    output: {
        filename: 'js/[name].js',
    },
    resolve: {
        modulesDirectories: ['node_modules', bower_dir],
        extensions: ['', '.webpack.js', '.web.js', '.js', '.jsx', '.json', 'scss'],
    },
    module: {
        loaders: [
            { test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader' },
            { test: /\.json$/, loader: 'json-loader' },
            { test: /\.txt$/, loader: 'raw-loader' },
            { test: /\.(png|jpg|jpeg|gif|svg|woff|woff2)(\?v=[0-9]\.[0-9]\.[0-9])?(\?[0-9a-zA-Z]*)?$/, loader: 'url-loader?limit=10000' },
            { test: /\.(eot|ttf|wav|mp3|otf)(\?v=[0-9]\.[0-9]\.[0-9])?(\?[0-9a-zA-Z]*)?$/, loader: 'file-loader' },
        ],
    },
    externals: {
        //  on the global var jQuery
        'jquery': 'jQuery'
    },
    plugins: [
        ...(PROD_ENV ? [
            new webpack.optimize.UglifyJsPlugin({
                compress: { warnings: false }
            })
        ] : []),
    ],
};

/*
* Sass compile task
* ~~~~~~~~~~~~~~~~~
*/
gulp.task('sass', function () {
    return gulp.src( [main_sass, editor_sass] )
    .pipe(sassGlob())
    .pipe(sass({
        includePaths: [bower_dir, npm_dir]
    }))
    .on('error', function(e) {
        notifier.notify({
            message:'Gulp-Sass compilation error. See console.',
            sound: true,
            wait: true
        });
        gutil.log(e);
        this.emit('end');
    })
    .pipe(gulp.dest(path.join(build_dir, 'css')))
    .pipe(livereload())
});


/*
* Scripts bundle
* ~~~~~~~~~~~~~~
*/
gulp.task('scripts', function () {
    return gulp.src([
        js_dir + '/index.js'
    ])
    .pipe(named())
    .pipe(
        webpackStream(webpackConfig)
        .on('error', function(e) {
            notifier.notify({
                message:'Gulp-Scripts compilation error. See console.',
                sound: true,
                wait: true
            });
            gutil.log(e);
            this.emit('end');
        })
    )
    .pipe(livereload())
    .pipe(gulp.dest(build_dir));
});


/*
* Copy JS Vendors like jQuery in build directory
*/
gulp.task('scripts:vendors', function () {
    return gulp.src(
        [
            path.join(js_dir, 'vendors', '/**/*'),
        ],
        { "base" : assets_dir }
    )
    .on('error', function(e) {
        notifier.notify({
            message:'Gulp-Copy error. See console.',
            sound: true,
            wait: true
        });
        gutil.log(e);
        this.emit('end');
    })
    .pipe(gulp.dest( path.join(build_dir) ));
});


/*
* Iconfont.
* ~~~~~~~~
*/
gulp.task('medias:iconfont', function(){
  let runTimestamp = Math.round(Date.now()/1000);
  console.log(font_dir + '/iconfont/');
  return gulp.src([iconfont_dir + '/**/*.svg'])
    .pipe(iconfont({
      fontName: 'iconfont',
      formats: ['ttf', 'eot', 'woff', 'svg'],
      normalize: true,
      fontHeight: 1001,
      timestamp: runTimestamp
    }))
    .on('glyphs', function(glyphs, options) {
      // CSS templating, e.g.
      gulp.src(iconfont_dir + '/template.scss')
        .pipe(consolidate('lodash', {
          glyphs: glyphs,
          fontName: 'iconfont',
          fontPath: path.join('../fonts/iconfont/'),
          className: 'iconfont'
        }))
        .pipe(rename('_iconfont.scss'))
        .pipe(gulp.dest(sass_dir + '/ui/components/'));
    })
    .pipe(gulp.dest(font_dir + '/iconfont/'));
});


/*
* Image compression
* ~~~~~~~~~~~~~~~~~
*/
gulp.task('medias:images', function() {
    return gulp.src(
        [
            path.join(img_dir, '/**/*'),
            path.join(svg_dir, '/**/*')
        ],
        { "base" : assets_dir }
    )
    .pipe(
        gulpif(PROD_ENV, imagemin({ verbose: true }))
    )
    .on('error', function(e) {
        notifier.notify({
            message: 'Gulp-Imagein compilation error. See console.',
            sound: true,
            wait: true
        });
        gutil.log(e);
        this.emit('end');
    })
    .pipe(gulp.dest( path.join(build_dir) ));
});


/*
* Various medias copy to distribution folder
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
gulp.task('medias:clean', function() {
    return del(
        [
            path.join(build_dir, 'img/**/*'),
            path.join(build_dir, 'svg/**/*'),
            path.join(build_dir, 'fonts/**/*')
        ]
    );
});


gulp.task('medias:copy', function() {
    return gulp.src(
        [
            path.join(font_dir, '/**/*'),
        ],
        { "base" : assets_dir }
    )
    .on('error', function(e) {
        notifier.notify({
            message:'Gulp-Copy error. See console.',
            sound: true,
            wait: true
        });
        gutil.log(e);
        this.emit('end');
    })
    .pipe(gulp.dest( path.join(build_dir) ));
});



/*
* Create theme style.css
* ~~~~~~~~~~~~~~~~~~~~~~
*/
gulp.task('theme', function () {
    return gulp.src( path.join(assets_dir, 'style.css') )
    .pipe(header(banner.theme))
    .pipe(gulp.dest(root_dir))
});


/*
* Global tasks
* ~~~~~~~~~~~~
*/
gulp.task('build', [
    'medias:iconfont',
    'medias:copy',
    'medias:images',
    'sass',
    'scripts',
    'scripts:vendors'
]);



/*
*  watch command, run build before
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/
gulp.task('default', ['build'], function() {

    notifier.notify({
        message: 'Now watching',
        sound: 'Tink',
    });

    livereload.listen({ reloadPage: true });

    // watch PHP files
    gulp.watch('**/*.php', livereload.reload);

    // watch any less file /css directory, ** is for recursive mode
    gulp.watch(sass_dir + '/**/*.scss', ['medias:iconfont', 'medias:copy', 'sass']);

    // watch any less file /js directory, ** is for recursive mode
    gulp.watch(js_dir + '/**/*.js', ['scripts', 'scripts:vendors']);

});
